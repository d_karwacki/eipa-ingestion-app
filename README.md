### Requirements
Application require Scala version 2.13.4 and SBT version 1.4.4.

### Configuration
Before starting the application you need to set ```eipa.api-key``` and ```downstream.url``` parameters inside "src/main/resources/application.conf" file.

### Launching
Application can be run using ```sbt run``` command from "eipa-ingestion-app" dir.

Alternatively it can be packaged to fat-jar using ```sbt assembly``` and run using
```java -jar ./target/scala-2.13/eipa-ingestion-app_2.13-0.1.jar``` 
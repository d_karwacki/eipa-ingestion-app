package actors

import java.time.{ZoneId, ZonedDateTime}

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.model.HttpResponse
import api.ClientHandler
import config.Config
import json.JsonSupport
import models.input.{ChargingPointInfo, EipaResponse, Price, Status}
import org.mockito.Mockito.when
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.Future
import scala.concurrent.duration._

class EIPAIngestionActorTest extends ScalaTestWithActorTestKit
  with JsonSupport
  with AnyWordSpecLike
  with Matchers
  with MockitoSugar {

  implicit val ec = system.executionContext


  val TestConfig = new Config {
    override lazy val eipaUrl = "eipa_url"
    override lazy val downstreamConsumerUrl = "downstream_url"
    override val requestInterval = 2.second
  }

  val Ts = ZonedDateTime.of(2020, 10, 10, 2, 0, 0, 0, ZoneId.systemDefault())
  val OriginalTs = ZonedDateTime.of(2020, 10, 10, 0, 0, 0, 0, ZoneId.systemDefault())
  val Prices = List(Price(Some("12"), None, None, OriginalTs))
  val Data = List(ChargingPointInfo(1, None, "code", Prices, None))
  val Data2 = List(ChargingPointInfo(2, None, "code2", Prices, Some(Status(1, 1, Ts))))
  val EIPAResponse1 = EipaResponse(Data, Ts)
  val EIPAResponse2 = EipaResponse(Data2, Ts)

  val EIPASuccessResponse = Future.successful(EIPAResponse1)
  val EIPASuccessResponse2 = Future.successful(EIPAResponse2)
  val DownstreamSuccessResponse = Future.successful(HttpResponse())
  val FailureResponse = Future.failed(new Exception("something went wrong"))

  val ClientHandler = mock[ClientHandler]
  val Behavior = EIPAIngestionActor(ClientHandler, TestConfig)

  import EIPAIngestionActor._

  "EIPAIngestionActor" when {
    "updating downstream service with EIPA events" should {
      "send a request to the EIPA endpoint" in {
        //given
        val probe = createTestProbe[Command]
        val ref = spawn(Behaviors.monitor(probe.ref, Behavior))

        //when
        ref ! ProvideEIPAEventsToDownstream
        //then
        probe.expectMessage(ProvideEIPAEventsToDownstream)
      }

      "receive successful response from EIPA if there wasn't any problems with the request" in {
        //given
        val probe = createTestProbe[Command]
        val ref = spawn(Behaviors.monitor(probe.ref, Behavior))

        when(ClientHandler.sendEIPARequest()).thenReturn(EIPASuccessResponse)
        //when
        ref ! ProvideEIPAEventsToDownstream
        //then
        probe.expectMessage(ProvideEIPAEventsToDownstream)
        probe.expectMessageType[FetchSuccess]
      }

      "receive response with failure from EIPA if there was some problem with the request" in {
        //given
        val probe = createTestProbe[Command]
        val ref = spawn(Behaviors.monitor(probe.ref, Behavior))

        when(ClientHandler.sendEIPARequest()).thenReturn(FailureResponse)

        //when
        ref ! ProvideEIPAEventsToDownstream
        //then
        probe.expectMessage(ProvideEIPAEventsToDownstream)
        probe.expectMessageType[FetchFailure]
      }

      "receive successful response from downstream service if there wasn't any problems with the request" in {
        //given
        val probe = createTestProbe[Command]
        val ref = spawn(Behaviors.monitor(probe.ref, Behavior))

        when(ClientHandler.sendEIPARequest()).thenReturn(EIPASuccessResponse)
        when(ClientHandler.sendDownstreamRequest(Data)).thenReturn(FailureResponse)

        //when
        ref ! ProvideEIPAEventsToDownstream
        //then
        probe.expectMessage(ProvideEIPAEventsToDownstream)
        probe.expectMessageType[FetchSuccess]
        probe.expectMessageType[UpdateFailure]
      }

      "receive response with failure from downstream service if there was some problem with the request" in {
        //given
        val probe = createTestProbe[Command]
        val ref = spawn(Behaviors.monitor(probe.ref, Behavior))

        when(ClientHandler.sendEIPARequest()).thenReturn(EIPASuccessResponse)
        when(ClientHandler.sendDownstreamRequest(Data)).thenReturn(DownstreamSuccessResponse)

        //when
        ref ! ProvideEIPAEventsToDownstream
        //then
        probe.expectMessage(ProvideEIPAEventsToDownstream)
        probe.expectMessageType[FetchSuccess]
        probe.expectMessage(UpdateSuccess)
      }

      "update downstream only if fetched Data is not in the cache already" in {
        //given
        val probe = createTestProbe[Command]

        val cache = Map(Data.head.code -> Data.head)
        val ref = spawn(Behaviors.monitor(probe.ref, EIPAIngestionActor(ClientHandler,TestConfig,cache)))

        when(ClientHandler.sendEIPARequest()).thenReturn(EIPASuccessResponse, EIPASuccessResponse2)
        when(ClientHandler.sendDownstreamRequest(Data)).thenReturn(DownstreamSuccessResponse)
        when(ClientHandler.sendDownstreamRequest(Data2)).thenReturn(DownstreamSuccessResponse)
        //when
        ref ! ProvideEIPAEventsToDownstream
        //then
        probe.expectMessage(ProvideEIPAEventsToDownstream)
        probe.expectMessageType[FetchSuccess]
        probe.expectMessage(ProvideEIPAEventsToDownstream)
        probe.expectMessageType[FetchSuccess]
        probe.expectMessage(UpdateSuccess)

      }
    }
  }

}

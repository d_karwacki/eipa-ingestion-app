package api

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding.{Get, Post}
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.unmarshalling.Unmarshal
import config.Config
import json.JsonSupport
import models.input.{ChargingPointInfo, EipaResponse}
import models.output.{ChargingPointStatusEvent, ChargingPointStatusEvents}

import scala.concurrent.{ExecutionContext, Future}


class ClientHandler(config: Config) extends JsonSupport {
  def sendEIPARequest()(implicit system: ActorSystem[_], ec: ExecutionContext): Future[EipaResponse] = {
    Http(system).singleRequest(Get(config.eipaUrl)).flatMap(r => Unmarshal(r).to[EipaResponse])
  }

  def sendDownstreamRequest(newData: List[ChargingPointInfo])(implicit system: ActorSystem[_], ec: ExecutionContext): Future[HttpResponse] = {
    val events = newData.map(ChargingPointStatusEvent.from)
    val request = Post(config.downstreamConsumerUrl, ChargingPointStatusEvents(events))
    Http(system).singleRequest(request)
  }
}


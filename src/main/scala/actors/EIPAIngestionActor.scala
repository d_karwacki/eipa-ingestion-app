package actors

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorSystem, Behavior}
import akka.http.scaladsl.model.StatusCodes.OK
import api.ClientHandler
import config.Config
import models.input.{ChargingPointInfo, EipaResponse}

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object EIPAIngestionActor {

  sealed trait Command
  final case object ProvideEIPAEventsToDownstream extends Command

  sealed trait EIPAFetchResponseCommand extends Command
  final case class FetchSuccess(response: EipaResponse) extends EIPAFetchResponseCommand
  final case class FetchFailure(message: String) extends EIPAFetchResponseCommand

  sealed trait DownstreamUpdateResponseCommand extends Command
  final case object UpdateSuccess extends DownstreamUpdateResponseCommand
  final case class UpdateFailure(message: String) extends DownstreamUpdateResponseCommand

  def apply(clientHandler: ClientHandler, config: Config, cache: Map[String, ChargingPointInfo] = Map.empty)(implicit ec: ExecutionContext, system: ActorSystem[_]): Behavior[Command] = new EIPAIngestionActor(clientHandler, config).active(cache)
}

class EIPAIngestionActor private(clientHandler: ClientHandler, config: Config)(implicit system: ActorSystem[_], ec: ExecutionContext) {

  import EIPAIngestionActor._

  def active(cache: Map[String, ChargingPointInfo]): Behavior[Command] = Behaviors.setup { context =>
    Behaviors.withTimers { timer =>
      timer.startTimerAtFixedRate(ProvideEIPAEventsToDownstream, config.requestInterval)
      Behaviors.receiveMessage {
        case ProvideEIPAEventsToDownstream =>
          context.log.info("Sending request to fetch new data from EIPA")
          val fetchResponse = clientHandler.sendEIPARequest()

          context.pipeToSelf(fetchResponse) {
            case Success(response) => FetchSuccess(response)
            case Failure(e) => FetchFailure(e.getMessage)
          }
          Behaviors.same

        case fetchResponse: EIPAFetchResponseCommand =>
          fetchResponse match {
            case FetchSuccess(response) =>
              context.log.info("Successfully fetched data from EIPA")

              val newData = response.data.filter(info => isAlreadyInCache(cache, info))
              val downstreamServiceResponse = clientHandler.sendDownstreamRequest(newData)

              if (newData.isEmpty) {
                Behaviors.same
              } else {
                context.pipeToSelf(downstreamServiceResponse) {
                  case Success(value) => if (value.status == OK) UpdateSuccess else UpdateFailure(s"Wrong status code: ${value.status}")
                  case Failure(e) => UpdateFailure(e.getMessage)
                }
                active(updatedCache(cache, newData))
              }

            case FetchFailure(message) =>
              context.log.warn("Something went wrong while pooling the message:" + message)
              Behaviors.same
          }

        case downstreamUpdateCommand: DownstreamUpdateResponseCommand =>
          downstreamUpdateCommand match {
            case UpdateSuccess =>
              context.log.info("Successfully updated downstream service")
              Behaviors.same

            case UpdateFailure(message) =>
              context.log.warn("Something went wrong while updating downstream service state: " + message)
              Behaviors.same
          }
      }
    }
  }


  def updatedCache(cache: Map[String, ChargingPointInfo], filteredData: List[ChargingPointInfo]): Map[String, ChargingPointInfo] = {
    cache ++ filteredData.map(info => info.code -> info).toMap
  }

  def isAlreadyInCache(cache: Map[String, ChargingPointInfo], info: ChargingPointInfo) = {
    !cache.get(info.code).exists(_.status == info.status)
  }
}
package json

import java.time.format.DateTimeFormatter
import java.time.{ZoneId, ZonedDateTime}
import java.util.UUID

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import models.input.{ChargingPointInfo, EipaResponse, Price, Status}
import models.output.ChargingPointStatus.ChargingPointStatus
import models.output.{ChargingPointEventData, ChargingPointStatus, ChargingPointStatusEvent, ChargingPointStatusEvents}
import spray.json.{DefaultJsonProtocol, JsString, JsValue, JsonFormat, RootJsonFormat, deserializationError}

import scala.util.Try

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {

  implicit object ZonedDateTimeProtocol extends RootJsonFormat[ZonedDateTime] {
    private val readFormatter = DateTimeFormatter.ISO_ZONED_DATE_TIME.withZone(ZoneId.systemDefault)
    private val writeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SS'Z'").withZone(ZoneId.systemDefault)

    override def write(dateTime: ZonedDateTime): JsValue = JsString(writeFormatter.format(dateTime))

    override def read(value: JsValue): ZonedDateTime = value match {
      case JsString(s) => Try(ZonedDateTime.parse(s, readFormatter)).getOrElse(error(s))
      case _ => error(value.toString())
    }

    def error(e: Any): ZonedDateTime = {
      val example = readFormatter.format(ZonedDateTime.now())
      deserializationError(f"'$e' is not a valid date value. Dates must be in compact ISO-8601 format, e.g. '$example'")
    }
  }

  implicit object uuidFormat extends JsonFormat[UUID] {
    override def write(uuid: UUID) = JsString(uuid.toString)

    override def read(value: JsValue): UUID = value match {
      case JsString(uuid) => UUID.fromString(uuid)
      case r => deserializationError("Expected UUID as JsString, but got " + r.getClass)
    }
  }

  implicit object ChargingPointStatusFormat extends RootJsonFormat[ChargingPointStatus] {
    override def write(status: ChargingPointStatus) = JsString(status.toString)

    override def read(value: JsValue): ChargingPointStatus = value match {
      case JsString(str) => ChargingPointStatus.withName(str)
      case r => deserializationError("Expected JsString, but got " + r.getClass)
    }
  }

  implicit val pricesJsonFormat = jsonFormat4(Price)
  implicit val statusJsonFormat = jsonFormat3(Status)
  implicit val chargingPointInfoFormat = jsonFormat5(ChargingPointInfo)
  implicit val eipaResponseFormat = jsonFormat2(EipaResponse)
  implicit val eventData = jsonFormat3(ChargingPointEventData)
  implicit val eventFormat = jsonFormat4(ChargingPointStatusEvent.apply)
  implicit val eventsFormat = jsonFormat1(ChargingPointStatusEvents)

}

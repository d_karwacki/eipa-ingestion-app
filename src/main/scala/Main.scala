import actors.EIPAIngestionActor
import actors.EIPAIngestionActor.ProvideEIPAEventsToDownstream
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorSystem, Behavior}
import api.ClientHandler
import config.Config
import json.JsonSupport
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContextExecutor

object Main extends JsonSupport {
  def apply[T](): Behavior[T] = {
    Behaviors.setup { context =>
      implicit lazy val system: ActorSystem[Nothing] = context.system
      implicit lazy val ec: ExecutionContextExecutor = system.executionContext

      lazy val config: Config = new Config
      val clientHandler = new ClientHandler(config)
      val ingestionActor = context.spawn(EIPAIngestionActor(clientHandler,config), "ingestion-actor")

      ingestionActor ! ProvideEIPAEventsToDownstream
      Behaviors.empty
    }
  }

  def main(args: Array[String]): Unit = {
    LoggerFactory.getLogger(getClass).info("Start")
    ActorSystem(Main(), "main")
  }

}

package config

import com.typesafe.config.ConfigFactory

import scala.concurrent.duration._

class Config {

  private lazy val config = ConfigFactory.load()

  lazy val eipaUrl: String = config.getString("eipa.url") + "/" + config.getString("eipa.api-key")
  lazy val downstreamConsumerUrl: String = config.getString("downstream.url")

  val requestInterval = config.getInt("eipa.request.interval").seconds
}
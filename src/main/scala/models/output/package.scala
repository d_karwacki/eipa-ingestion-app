package models

import java.time.ZonedDateTime
import java.util.UUID

import models.input.ChargingPointInfo

package object output {

  object ChargingPointStatus extends Enumeration {
    type ChargingPointStatus = Value
    val Available = Value("AVAILABLE")
    val Occupied = Value("OCCUPIED")
    val OutOfOrder = Value("OUT_OF_ORDER")
    val Unknown = Value("UNKNOWN")
  }

  case class ChargingPointStatusEvents(events: List[ChargingPointStatusEvent])

  case class ChargingPointEventData(pointId: Int,
                                    status: Option[ChargingPointStatus.Value],
                                    originTs: Option[ZonedDateTime])

  case class ChargingPointStatusEvent(data: ChargingPointEventData,
                                      id: UUID = UUID.randomUUID(),
                                      ts: ZonedDateTime = ZonedDateTime.now(),
                                      `type`: String = "STATUS_UPDATE")

  object ChargingPointStatusEvent {
    def from(p: ChargingPointInfo): ChargingPointStatusEvent = {
      val data = ChargingPointEventData(pointId = p.point_id,
        status = p.status.map(_.toChargingPointStatus),
        originTs = p.status.map(_.ts)
      )

      ChargingPointStatusEvent(data)
    }
  }

}

package models

import java.time.ZonedDateTime

import models.output.ChargingPointStatus.{Available, ChargingPointStatus, Occupied, OutOfOrder, Unknown}

package object input {

  case class Price(price: Option[String], unit: Option[String], literal: Option[String], ts: ZonedDateTime)

  case class Status(availability: Int, status: Int, ts: ZonedDateTime) {
    def toChargingPointStatus: ChargingPointStatus = {
      (status, availability) match {
        case (1, 1) => Available
        case (1, 0) => Occupied
        case (0, 1) => OutOfOrder
        case _ => Unknown
      }
    }
  }

  case class ChargingPointInfo(point_id: Int,
                               max_output_unit: Option[String],
                               code: String,
                               prices: List[Price],
                               status: Option[Status])

  case class EipaResponse(data: List[ChargingPointInfo],
                          generated: ZonedDateTime)

}

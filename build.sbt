lazy val akkaVersion = "2.6.10"
lazy val akkaHttpVersion = "10.2.1"
lazy val scalaTestVersion = "3.2.2"
lazy val mockitoVersion = "3.2.2.0"
lazy val logbackVersion = "1.2.3"

lazy val akkaDependencies = Seq(
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion,
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-core" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % Test,
  "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test,
  "com.typesafe.akka" %% "akka-actor-testkit-typed" % akkaVersion % Test,
)


lazy val scalaTest = Seq(
  "org.scalactic" %% "scalactic" % scalaTestVersion,
  "org.scalatest" %% "scalatest" % scalaTestVersion % "test"
)

lazy val mockito = Seq(
  "org.scalatestplus" %% "mockito-3-4" % mockitoVersion % "test"
)

lazy val logback = Seq(
  "ch.qos.logback" % "logback-classic" % logbackVersion
)

lazy val root = (project in file("."))
  .settings(
    name := "eipa-ingestion-app",
    version := "0.1",
    scalaVersion := "2.13.4",
    libraryDependencies ++= akkaDependencies
      ++ scalaTest
      ++ mockito
      ++ logback
  )
